package com.example.andresoyarce.pasapalabra;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

public class DisplayMessageActivity extends AppCompatActivity {

    public static final String EXTRA_MESSAGE = "com.example.andresoyarce.MESSAGE";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_display_message);

    }

    /** Called when the user taps the Send button */
    public void checkPreguntas(View view) {
        // Do something in response to button
        Intent intent = new Intent(this, revision_Preguntas.class);
        String message = "pasapalabra";
        intent.putExtra(EXTRA_MESSAGE, message);
        startActivity(intent);

    }
}
